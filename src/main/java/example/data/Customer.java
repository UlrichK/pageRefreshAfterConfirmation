package example.data;

import java.io.Serializable;

/**
 * <p>
 * Created by ulrich.knaack on 02.12.2016.
 * </p>
 */
public class Customer implements Serializable
{
    private static final long serialVersionUID = -6460070193586521308L;
    private long id;
    private String name = "default Name";
    private String town = "default Town";

    public Customer()
    {
    }

    /**
     * Constructor
     *
     * @param name
     * @param town
     */
    public Customer(String name, String town)
    {
        this.name = name;
        this.town = town;
    }

    public Customer(long id, String name, String town)
    {
        this.name = name;
        this.town = town;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTown()
    {
        return town;
    }

    public void setTown(String town)
    {
        this.town = town;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        Customer other = (Customer) obj;
        if (id != other.id)
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Customer{" + "id=" + id + ", name='" + name + '\'' + ", town='" + town + '\'' + '}';
    }
}
