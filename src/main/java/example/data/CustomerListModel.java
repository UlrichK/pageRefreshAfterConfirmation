package example.data;

import java.util.List;

import org.apache.wicket.model.LoadableDetachableModel;

public class CustomerListModel extends LoadableDetachableModel<List<Customer>>
{

    private static final long serialVersionUID = 1L;
    private final CustomerFilter filter = new CustomerFilter();

    public CustomerListModel()
    {
        // TODO Auto-generated constructor stub
    }

    @Override
    protected List<Customer> load()
    {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return the search
     */
    public CustomerFilter getFilter()
    {
        return filter;
    }

}
