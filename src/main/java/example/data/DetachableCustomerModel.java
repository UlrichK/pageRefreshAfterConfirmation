/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example.data;

import org.apache.wicket.model.LoadableDetachableModel;

/**
 * detachable model for an instance of customer
 */
public class DetachableCustomerModel extends LoadableDetachableModel<Customer>
{
    private static final long serialVersionUID = 1L;
    private final long id;

    protected CustomerDatabase getCustomerDB()
    {
        return DatabaseLocator.getDatabase();
    }

    public DetachableCustomerModel()
    {
        this(0);
    }

    /**
     * @param id
     */
    public DetachableCustomerModel(long id)
    {
        this.id = id;
    }

    public DetachableCustomerModel(Customer c)
    {
        this(c.getId());
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return Long.valueOf(id).hashCode();
    }

    /**
     * used for dataview with ReuseIfModelsEqualStrategy item reuse strategy
     *
     * @see org.apache.wicket.markup.repeater.ReuseIfModelsEqualStrategy
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object obj)
    {
        if (obj == this)
        {
            return true;
        }
        else if (obj == null)
        {
            return false;
        }
        else if (obj instanceof DetachableCustomerModel)
        {
            DetachableCustomerModel other = (DetachableCustomerModel) obj;
            return other.id == id;
        }
        return false;
    }

    /**
     * @see LoadableDetachableModel#load()
     */
    @Override
    protected Customer load()
    {
        if (this.id == 0)
        {
            return new Customer();
        }
        return getCustomerDB().get(id);
    }
}
