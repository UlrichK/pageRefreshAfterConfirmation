/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example.data;

import java.util.Collection;


/**
 * generates random customers
 */
public class CustomerGenerator
{
    private static CustomerGenerator instance = new CustomerGenerator();
    private static long nextId = 1;

    /**
     * @return static instance of generator
     */
    public static CustomerGenerator getInstance()
    {
        return instance;
    }

    private final String[] towns = {"Hannover", "Frankfurt", "Hamburg", "Bremen", "Minden", "Leer",
            "Aurich", "Osterode", "Northeim", "Hameln", "Soltau", "Celle", "Uelzen", "Dannenberg",
            "Lingen", "Nordhorn", "Emden", "Cuxhaven", "Garbsen", "Burgwedel", "Gifhorn",
            "Wolfsburg", "Braunschweig", "Salzgitter", "Burgdorf", "Uetze", "Peine", "Wunstorf",
            "Bad Nenndorf"};
    private final String[] names = {"Mueller", "Meier", "Schulz", "Schulze", "Franke", "Bauer",
            "Rose", "Knaak", "Bischoff", "Ziems", "Reus", "Klaus", "Pauly", "Kohl", "Senft",
            "Funke", "Neumann", "Meier", "Wald", "Fischer", "Kreuzer", "Mann", "Puster", "Weng",
            "Ziems"};

    private CustomerGenerator()
    {

    }

    /**
     * @return unique id
     */
    public synchronized long generateId()
    {
        return nextId++;
    }

    /**
     * generates a new contact
     *
     * @return generated contact
     */
    public Customer generate()
    {
        Customer customer = new Customer(randomString(names), randomString(towns));
        customer.setId(generateId());

        return customer;
    }

    public void generate(Collection<Customer> collection, int count)
    {
        for (int i = 0; i < count; i++)
        {
            collection.add(generate());
        }
    }

    private int rint(int min, int max)
    {
        return (int) (Math.random() * (max - min) + min);
    }

    private String randomString(String[] choices)
    {
        return choices[rint(0, choices.length)];
    }

}
