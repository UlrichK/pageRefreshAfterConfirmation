/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example.data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

// Pojo für Filter
public class CustomerFilter implements Serializable
{
    private static final long serialVersionUID = 1L;
    private long id;
    private String name;
    private String town;
    private boolean asc = true;
    private List<String> orderAttributes;

    /**
     * @return the id
     */
    public long getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the town
     */
    public String getTown()
    {
        return town;
    }

    /**
     * @param town the town to set
     */
    public void setTown(String town)
    {
        this.town = town;
    }

    /**
     * @return the asc
     */
    public boolean isAsc()
    {
        return asc;
    }

    /**
     * @param asc the asc to set
     */
    public void setAsc(boolean asc)
    {
        this.asc = asc;
    }

    /**
     * @return the orderAttributes
     */
    public List<String> getOrderAttributes()
    {
        return orderAttributes;
    }

    /**
     * @param orderAttributes the orderAttributes to set
     */
    public void setOrderAttributes(String... orderAttributes)
    {
        this.orderAttributes = Arrays.asList(orderAttributes);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((town == null) ? 0 : town.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        CustomerFilter other = (CustomerFilter) obj;
        if (id != other.id)
        {
            return false;
        }
        if (name == null)
        {
            if (other.name != null)
            {
                return false;
            }
        }
        else if (!name.equals(other.name))
        {
            return false;
        }
        if (town == null)
        {
            if (other.town != null)
            {
                return false;
            }
        }
        else if (!town.equals(other.town))
        {
            return false;
        }
        return true;
    }

}
