/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example.data;

import java.util.*;

import org.apache.wicket.extensions.markup.html.repeater.util.SortParam;



/**
 * simple database for Customers
 */
public class CustomerDatabase
{
    private final Map<Long, Customer> map = Collections
            .synchronizedMap(new HashMap<Long, Customer>());
    private final List<Customer> idIdx = Collections.synchronizedList(new ArrayList<Customer>());
    private final List<Customer> nameIdx = Collections.synchronizedList(new ArrayList<Customer>());
    private final List<Customer> townIdx = Collections.synchronizedList(new ArrayList<Customer>());
    private final List<Customer> idDescIdx = Collections
            .synchronizedList(new ArrayList<Customer>());
    private final List<Customer> nameDescIdx = Collections
            .synchronizedList(new ArrayList<Customer>());
    private final List<Customer> townDescIdx = Collections
            .synchronizedList(new ArrayList<Customer>());

    /**
     * Constructor
     *
     * @param count number of Customers to generate at startup
     */
    public CustomerDatabase(int count)
    {
        for (int i = 0; i < count; i++)
        {
            add(CustomerGenerator.getInstance().generate());
        }
        updateIndices();
    }

    /**
     * find Customer by id
     *
     * @param id
     *
     * @return Customer
     */
    public Customer get(Long id)
    {
        Customer c = map.get(id);
        if (c == null)
        {
            throw new RuntimeException("Customer with id [" + id + "] not found in the database");
        }
        return c;
    }

    protected void add(final Customer customer)
    {
        map.put(customer.getId(), customer);
        idIdx.add(customer);
        nameIdx.add(customer);
        nameDescIdx.add(customer);
        idDescIdx.add(customer);
        townIdx.add(customer);
        townDescIdx.add(customer);
    }

    /**
     * select Customers and apply sort
     *
     * @param first
     * @param count
     * @param sort
     *
     * @return list of Customers
     */
    public List<Customer> find(long first, long count, SortParam<String> sort)
    {
        return getIndex(sort).subList((int) first, (int) (first + count));
    }

    public List<Customer> getIndex(SortParam<String> sort)
    {
        if (sort == null)
        {
            return nameIdx;
        }

        if (sort.getProperty().equals("name"))
        {
            return sort.isAscending() ? nameIdx : nameDescIdx;
        }
        else if (sort.getProperty().equals("town"))
        {
            return sort.isAscending() ? townIdx : townDescIdx;
        }
        else if (sort.getProperty().equals("id"))
        {
            return sort.isAscending() ? idIdx : idDescIdx;
        }
        throw new RuntimeException(
                "unknown sort option [" + sort + "]. valid fields: [id], [name], [town]");
    }

    /**
     * @return number of Customers in the database
     */
    public int getCount()
    {
        return nameIdx.size();
    }

    /**
     * add Customer to the database
     *
     * @param customer
     */
    public void save(final Customer customer)
    {
        if (customer.getId() == 0)
        {
            customer.setId(CustomerGenerator.getInstance().generateId());
            add(customer);
            updateIndices();
        }
        else
        {
            if (map.get(customer.getId()) != null)
            {
                map.get(customer.getId()).setName(customer.getName());
                map.get(customer.getId()).setTown(customer.getTown());
            }
        }
        updateIndices();
    }

    /**
     * delete Customer from the database
     *
     * @param customer
     */
    public void delete(Customer customer)
    {
        map.remove(customer.getId());
        idIdx.remove(customer);
        nameIdx.remove(customer);
        townIdx.remove(customer);
        idDescIdx.remove(customer);
        nameDescIdx.remove(customer);
        townDescIdx.remove(customer);
        updateIndices();
    }

    private void updateIndices()
    {
        Collections.sort(idIdx, new Comparator<Customer>()
        {
            @Override
            public int compare(Customer arg0, Customer arg1)
            {
                if (arg0.getId() < arg1.getId())
                {
                    return -1;
                }
                else if (arg0.getId() > arg1.getId())
                {
                    return 1;
                }
                else
                {
                }
                return 0;
            }
        });

        Collections.sort(nameIdx, new Comparator<Customer>()
        {
            @Override
            public int compare(Customer arg0, Customer arg1)
            {
                return (arg0).getName().compareTo((arg1).getName());
            }
        });

        Collections.sort(townIdx, new Comparator<Customer>()
        {
            @Override
            public int compare(Customer arg0, Customer arg1)
            {
                return (arg0).getTown().compareTo((arg1).getTown());
            }
        });

        Collections.sort(idDescIdx, new Comparator<Customer>()
        {
            @Override
            public int compare(Customer arg0, Customer arg1)
            {
                if (arg1.getId() < arg0.getId())
                {
                    return -1;
                }
                else if (arg1.getId() > arg0.getId())
                {
                    return 1;
                }
                else
                {
                }
                return 0;
            }
        });

        Collections.sort(nameDescIdx, new Comparator<Customer>()
        {
            @Override
            public int compare(Customer arg0, Customer arg1)
            {
                return (arg1).getName().compareTo((arg0).getName());
            }
        });

        Collections.sort(townDescIdx, new Comparator<Customer>()
        {
            @Override
            public int compare(Customer arg0, Customer arg1)
            {
                return (arg1).getTown().compareTo((arg0).getTown());
            }
        });

    }

}
