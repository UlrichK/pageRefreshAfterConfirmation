
package example.data;

import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;

// DataProvider for DataTable (For Sorting and filtering)

public class CustomerDataProvider extends SortableDataProvider<Customer, String>
        implements IFilterStateLocator<CustomerFilter>
{

    public CustomerDataProvider()
    {
        super();
        setSort("id", SortOrder.ASCENDING);
    }

    private static final long serialVersionUID = 1L;
    private CustomerFilter state = new CustomerFilter();

    protected CustomerDatabase getCustomerDB()
    {
        return DatabaseLocator.getDatabase();
    }

    /**
     * retrieves customers from database starting with index <code>first</code>
     * and ending with <code>first+count</code>
     *
     * @see org.apache.wicket.markup.repeater.data.IDataProvider#iterator(long, long)
     */
    @Override
    public Iterator<Customer> iterator(long first, long count)
    {

        List<Customer> customersFound = getCustomerDB().getIndex(getSort());
        return filterCustomers(customersFound).subList((int) first, (int) (first + count))
                .iterator();

    }

    private List<Customer> filterCustomers(List<Customer> customersFound)
    {
        ArrayList<Customer> result = new ArrayList<Customer>();
        String sname = getFilterState().getName();
        String stown = getFilterState().getTown();

        for (Customer customer : customersFound)
        {
            Boolean add = true;
            if (sname != null && !StringUtils.contains(customer.getName(), sname))
            {
                add = false;
            }
            if (stown != null && !StringUtils.contains(customer.getTown(), stown))
            {
                add = false;
            }
            if (add)
            {
                result.add(customer);
            }
        }
        return result;
    }

    /**
     * returns total number of customer in the database
     *
     * @see org.apache.wicket.markup.repeater.data.IDataProvider#size()
     */
    @Override
    public long size()
    {
        return filterCustomers(getCustomerDB().getIndex(getSort())).size();
    }

    /**
     * wraps retrieved customer pojo with a wicket model
     *
     * @see org.apache.wicket.markup.repeater.data.IDataProvider#model(Object)
     */
    @Override
    public IModel<Customer> model(Customer object)
    {
        return new DetachableCustomerModel(object);
    }

    /**
     * @see org.apache.wicket.model.IDetachable#detach()
     */
    @Override
    public void detach()
    {
    }

    @Override
    public CustomerFilter getFilterState()
    {
        return state;
    }

    @Override
    public void setFilterState(CustomerFilter state)
    {
        this.state = state;
    }

}
