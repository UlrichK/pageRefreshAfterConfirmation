package example.confirm;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

import de.agilecoders.wicket.core.markup.html.bootstrap.button.BootstrapAjaxButton;
import de.agilecoders.wicket.core.markup.html.bootstrap.button.Buttons;
import de.agilecoders.wicket.core.markup.html.bootstrap.image.GlyphIconType;

/**
 * <p>Created by ulrich.knaack on 07.07.2017.</p>
 */
public abstract class DeleteObjectPanel extends Panel
{
    private static final long serialVersionUID = -1020348444499080854L;

    private final ConfirmDeleteModal confirmDelete;

    public DeleteObjectPanel(String id, String entryInfo)
    {
        super(id);
        setOutputMarkupId(true);

        BootstrapAjaxButton deleteBtn = new BootstrapAjaxButton("delete", Buttons.Type.Warning)
        {
            private static final long serialVersionUID = -7459788175129883354L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                confirmDelete.appendShowDialogJavaScript(target);
                target.add(DeleteObjectPanel.this);
            }
        };
        deleteBtn.setIconType(GlyphIconType.trash);
        add(deleteBtn);

        confirmDelete = new ConfirmDeleteModal("confirmDelete", entryInfo)
        {
            private static final long serialVersionUID = -4587122820619019170L;

            @Override
            protected void deleteAfterConfirmation(AjaxRequestTarget target)
            {
                performDeletion(target);
                confirmDelete.appendCloseDialogJavaScript(target);
            }
        };
        confirmDelete.header(Model.of("Eintrag löschen"));
        confirmDelete.setOutputMarkupId(true);
        add(confirmDelete);

    }

    protected abstract void performDeletion(AjaxRequestTarget target);
}
