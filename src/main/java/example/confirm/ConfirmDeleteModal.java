package example.confirm;

import java.io.Serializable;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.Model;

import de.agilecoders.wicket.core.markup.html.bootstrap.button.BootstrapAjaxButton;
import de.agilecoders.wicket.core.markup.html.bootstrap.button.Buttons;
import de.agilecoders.wicket.core.markup.html.bootstrap.dialog.Modal;

import static de.agilecoders.wicket.core.markup.html.bootstrap.button.Buttons.Type.Danger;

/**
 * <p>Created by ulrich.knaack on 07.07.2017.</p>
 */
public abstract class ConfirmDeleteModal extends Modal<Void> implements Serializable
{
    private static final long serialVersionUID = 8988802531498977286L;

    public ConfirmDeleteModal(String markupId, String entryInfo)
    {
        super(markupId);

        Form form = new Form("confirmation");
        add(form);

        MultiLineLabel entryLabel = new MultiLineLabel("entryInfo", entryInfo);
        form.add(entryLabel);

        BootstrapAjaxButton delete = new BootstrapAjaxButton("delete"
                , Model.of("löschen"), Danger)
        {
            private static final long serialVersionUID = -5191266499691403976L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                deleteAfterConfirmation(target);
                super.onSubmit(target, form);
            }
        };
        form.add(delete);

        BootstrapAjaxButton cancel = new BootstrapAjaxButton("cancel"
                , Model.of("behalten"), Buttons.Type.Info)
        {
            private static final long serialVersionUID = 2037914657567616019L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                appendCloseDialogJavaScript(target);
            }
        };
        form.add(cancel);
    }

    protected abstract void deleteAfterConfirmation(AjaxRequestTarget target);
}
