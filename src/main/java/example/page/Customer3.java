package example.page;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.*;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.*;

import de.agilecoders.wicket.core.markup.html.bootstrap.button.*;
import de.agilecoders.wicket.core.markup.html.bootstrap.form.BootstrapForm;
import de.agilecoders.wicket.extensions.markup.html.bootstrap.table.BootstrapDefaultDataTable;
import example.confirm.DeleteObjectPanel;
import example.data.*;

/**
 * <p>
 * Created by andreas reiche
 * </p>
 */
public class Customer3 extends WebPage
{
    private static final long serialVersionUID = 1L;

    // Form für Filterfelder
    private final BootstrapForm<CustomerFilter> filterForm;

    // Dataprovider für DataTable
    private CustomerDataProvider dp = new CustomerDataProvider();

    // Bootstrap-Version der DefaultDataTable
    private BootstrapDefaultDataTable<Customer, String> dataTable;

    // Container um Seiteninhalt
    private final WebMarkupContainer container = new WebMarkupContainer("container");

    // Model für Filter
    private IModel<CustomerFilter> filterModel = new CompoundPropertyModel<CustomerFilter>(
            dp.getFilterState());

    // Form für Neu Button
    private Form<Customer> editForm = new Form<Customer>("editForm");

    // ModalDialog für Neuanlage
    private CustomerCreate3 createDialog;

    public Customer3()
    {

        container.setOutputMarkupId(true);
        add(container);
        filterForm = new BootstrapForm<CustomerFilter>("filter", filterModel);
        filterForm.add(new TextField<>("name"));
        filterForm.add(new TextField<>("town"));
        filterForm.add(new BootstrapAjaxButton("filterButton", Model.of("Filtern"), filterForm,
                Buttons.Type.Info)
        {
            private static final long serialVersionUID = 1L;

            /*
             * (non-Javadoc)
             *
             * @see
             * org.apache.wicket.ajax.markup.html.form.AjaxButton#onSubmit(org.
             * apache.wicket.ajax.AjaxRequestTarget,
             * org.apache.wicket.markup.html.form.Form)
             */
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                dataTable.setCurrentPage(0);
                target.add(container);
            }

        });
        filterForm.add(new BootstrapAjaxButton("filterClearButton", Model.of("Filter entfernen"),
                filterForm, Buttons.Type.Info)
        {
            private static final long serialVersionUID = 1L;

            /*
             * (non-Javadoc)
             *
             * @see
             * org.apache.wicket.ajax.markup.html.form.AjaxButton#onSubmit(org.
             * apache.wicket.ajax.AjaxRequestTarget,
             * org.apache.wicket.markup.html.form.Form)
             */
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                // Filter zurücksetzen
                filterModel.getObject().setName(null);
                filterModel.getObject().setTown(null);
                dataTable.setCurrentPage(0);
                target.add(container);
            }

        });
        container.add(filterForm);

        // Liste für Spalten der DataTable
        List<IColumn<Customer, String>> columns = new ArrayList<>();
        columns.add(new PropertyColumn<Customer, String>(new Model<String>("ID"), "id", "id"));
        columns.add(
                new PropertyColumn<Customer, String>(new Model<String>("Name"), "name", "name"));
        columns.add(
                new PropertyColumn<Customer, String>(new Model<String>("Stadt"), "town", "town"));

        columns.add(
                new AbstractColumn<Customer, String>(new Model<>("Löschen"))
                {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void populateItem(Item<ICellPopulator<Customer>> cellItem,
                            String componentId, IModel<Customer> customerModel)
                    {
                        Customer customer = customerModel.getObject();
                        String customerInfo = "Kunde:\n"
                                + customer.getName();
                        cellItem.add(new DeleteObjectPanel(componentId, customerInfo)
                        {
                            private static final long serialVersionUID = 2408937138549967444L;

                            @Override
                            protected void performDeletion(AjaxRequestTarget target)
                            {
                                //customerBean.deleteByUuid(customer.getBopUuid());
                                DatabaseLocator.getDatabase().delete(customerModel.getObject());
                                //target.add(filterForm);  // Page is "disabled" completely
                                target.add(getPage());   // this works
                            }
                        });
                    }

                });
        dataTable = new BootstrapDefaultDataTable<Customer, String>("dataTable", columns, dp, 8);
        dataTable.setOutputMarkupId(true);
        filterForm.add(dataTable);

        editForm.setOutputMarkupId(true);
        container.add(editForm);
        BootstrapAjaxButton showNew = new BootstrapAjaxButton("showNew", Model.of("Neuer Kunde"),
                Buttons.Type.Info)
        {
            private static final long serialVersionUID = 1L;

            /*
             * (non-Javadoc)
             *
             * @see
             * org.apache.wicket.ajax.markup.html.form.AjaxButton#onSubmit(org.
             * apache.wicket.ajax.AjaxRequestTarget,
             * org.apache.wicket.markup.html.form.Form)
             */
            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                createDialog.setSubmitVisible(true).setSaveVisible(false)
                        .header(Model.of("Neuen Datensatz anlegen"))
                        .setDefaultModelObject(new Customer());
                createDialog.appendShowDialogJavaScript(target);
                target.add(container);
            }
        };
        editForm.add(showNew);
        createDialog = new CustomerCreate3("createDialog",
                new CompoundPropertyModel<Customer>(new DetachableCustomerModel(0)))
        {
            private static final long serialVersionUID = 9143976124881243743L;

            @Override
            public void createOk(AjaxRequestTarget target, IModel<Customer> customerModel)
            {
                DatabaseLocator.getDatabase().save(customerModel.getObject());
                filterModel.getObject().setName(customerModel.getObject().getName());
                setResponsePage(getPage());
            }

            @Override
            public void createCancel(AjaxRequestTarget target)
            {
                close(target);
            }

            @Override
            public void saveOk(AjaxRequestTarget target, IModel<Customer> customerModel)
            {
                // noob
            }
        };
        editForm.add(createDialog);
    }

}
