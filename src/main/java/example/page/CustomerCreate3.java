package example.page;

import java.io.Serializable;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import de.agilecoders.wicket.core.markup.html.bootstrap.button.BootstrapAjaxButton;
import de.agilecoders.wicket.core.markup.html.bootstrap.button.Buttons;
import de.agilecoders.wicket.core.markup.html.bootstrap.dialog.Modal;
import de.agilecoders.wicket.core.markup.html.bootstrap.form.BootstrapForm;
import example.data.Customer;

/**
 * <p>
 * Created by andreas.reiche.
 * </p>
 */
public abstract class CustomerCreate3 extends Modal<Customer> implements Serializable
{
    private static final long serialVersionUID = 1L;
    private BootstrapAjaxButton submit;
    private BootstrapAjaxButton save;
    private BootstrapForm<Customer> form;

    public CustomerCreate3(String id, IModel<Customer> customerModel)
    {
        super(id, customerModel);
        setDefaultModel(customerModel);
        setOutputMarkupPlaceholderTag(true);

        form = new BootstrapForm<Customer>("customerForm");
        add(form);
        form.add(new TextField<String>("id").setEnabled(false));
        form.add(new TextField<String>("name"));
        form.add(new TextField<String>("town"));

        submit = new BootstrapAjaxButton("submitCustomer", Model.of("Anlegen"),
                Buttons.Type.Default)
        {

            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                createOk(target, customerModel);
            }
        };
        form.add(submit);

        save = new BootstrapAjaxButton("saveCustomer", Model.of("Speichern"),
                Buttons.Type.Default)
        {

            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                saveOk(target, customerModel);
            }
        };
        form.add(save);

        BootstrapAjaxButton cancel = new BootstrapAjaxButton("cancelCustomer",
                Model.of("Abbrechen"), Buttons.Type.Default)
        {

            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form)
            {
                super.onSubmit(target, form);
                createCancel(target);
            }
        };
        cancel.setDefaultFormProcessing(false);
        form.add(cancel);

    }

    public CustomerCreate3 setSubmitVisible(boolean visible)
    {
        submit.setVisible(visible);
        return this;
    }

    public CustomerCreate3 setSaveVisible(boolean visible)
    {
        save.setVisible(visible);
        return this;
    }

    public abstract void saveOk(AjaxRequestTarget target, IModel<Customer> customerModel);

    public abstract void createOk(AjaxRequestTarget target, IModel<Customer> customerModel);

    public abstract void createCancel(AjaxRequestTarget target);
}
