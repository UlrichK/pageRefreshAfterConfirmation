package example;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

import de.agilecoders.wicket.core.Bootstrap;
import de.agilecoders.wicket.core.settings.BootstrapSettings;
import example.data.CustomerDatabase;
import example.page.Customer3;

/**
 * <p>
 * Created by ulrich.knaack on 29.11.2016.
 * </p>
 */
public class BSApp extends WebApplication
{
    // private static Logger logger =
    // LogManager.getLogger(BSApp.class.getName());

    // Initialisiert die Database
    private final CustomerDatabase customerDB = new CustomerDatabase(25);

    @Override
    public Class<? extends Page> getHomePage()
    {
        return Customer3.class;
    }

    @Override
    protected void init()
    {
        super.init();
        BootstrapSettings settings = new BootstrapSettings();
        Bootstrap.install(this, settings);
    }

    /**
     * @return customer database
     */
    public CustomerDatabase getCustomerDB()
    {
        return customerDB;
    }

}
